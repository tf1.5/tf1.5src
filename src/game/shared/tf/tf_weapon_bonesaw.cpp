//====== Copyright � 1996-2005, Valve Corporation, All rights reserved. =======
//
// Purpose: 
//
//=============================================================================

#include "cbase.h"
#include "tf_weapon_bonesaw.h"

#ifdef GAME_DLL
	#include "tf_player.h"
	#include "engine/IEngineSound.h"
	#include "tf_gamestats.h"
#else
	#include "c_tf_player.h"
#endif
//=============================================================================
//
// Weapon Bonesaw tables.
//

IMPLEMENT_NETWORKCLASS_ALIASED( TFBonesaw, DT_TFWeaponBonesaw )

BEGIN_NETWORK_TABLE( CTFBonesaw, DT_TFWeaponBonesaw )
END_NETWORK_TABLE()

BEGIN_PREDICTION_DATA( CTFBonesaw )
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( tf_weapon_bonesaw, CTFBonesaw );
PRECACHE_WEAPON_REGISTER( tf_weapon_bonesaw );

//=============================================================================
//
// Weapon Syringe tables.
//

IMPLEMENT_NETWORKCLASS_ALIASED( TFSyringe, DT_TFWeaponSyringe )

BEGIN_NETWORK_TABLE( CTFSyringe, DT_TFWeaponSyringe )
END_NETWORK_TABLE()

BEGIN_PREDICTION_DATA( CTFSyringe )
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( tf_weapon_syringe, CTFSyringe );
PRECACHE_WEAPON_REGISTER( tf_weapon_syringe );

#define TF_SYRINGE_HEAL_SOUND	"Weapon_Syringe.Heal"

#ifdef GAME_DLL
void CTFSyringe::OnEntityHit( CBaseEntity* pOther )
{
	// i fucking love five guys

	CTFPlayer* pVictim = ToTFPlayer( pOther );
	CTFPlayer* pOwner = GetTFPlayerOwner();

	if ( !pOwner )
		return;

	if ( !pVictim )
		return;

	// Hit yourself. Idiot.
	if ( pOwner == pVictim )
		return;
	
	int iHealthNeeded = pVictim->GetMaxHealth() - pVictim->GetHealth();

	int iHealthIncreased = min(iHealthNeeded, m_pWeaponInfo->GetWeaponData(m_iWeaponMode).m_nHeal);
	
	if ( pVictim->GetTeamNumber() == pOwner->GetTeamNumber() && pVictim->GetHealth() <= pVictim->GetMaxHealth()  )
	{
		if (iHealthNeeded > 0)
		{
			pVictim->TakeHealth( iHealthIncreased, DMG_GENERIC );

			CPASAttenuationFilter filter(pVictim->WorldSpaceCenter());
			EmitSound(filter, entindex(), TF_SYRINGE_HEAL_SOUND);
			
			CTF_GameStats.Event_PlayerHealedOther(pOwner, iHealthIncreased, false);

			IGameEvent* event = gameeventmanager->CreateEvent("player_healed");
			if (event)
			{
				event->SetInt("priority", 1);
				event->SetInt("patient", pVictim->GetUserID());
				event->SetInt("healer", pOwner->GetUserID());
				event->SetInt("amount", iHealthIncreased);

				gameeventmanager->FireEvent(event);
			}
		}
		

		/*
		if ( pVictim->GetHealth() >= pVictim->GetMaxHealth() )
		{
			int iOverhealAmount = min( ( pVictim->m_Shared.GetMaxBuffedHealth() - pVictim->GetHealth() ), 5 );
			
			pVictim->TakeHealth(iOverhealAmount, DMG_GENERIC);

			CPASAttenuationFilter filter(pVictim->WorldSpaceCenter());
			EmitSound(filter, entindex(), TF_SYRINGE_HEAL_SOUND);

			CTF_GameStats.Event_PlayerHealedOther(pOwner, 5, false);

			IGameEvent* event =	gameeventmanager->CreateEvent( "player_healed" );
			if ( event )
			{
				event->SetInt("priority", 1);
				event->SetInt("patient", pVictim->GetUserID());
				event->SetInt("healer", pOwner->GetUserID());
				event->SetInt("amount", iHealthIncreased);

				gameeventmanager->FireEvent(event);
			}
		}
		*/

	}
	else
	{
		//pVictim->m_Shared.Infect( pOwner );
	}


}
#endif
