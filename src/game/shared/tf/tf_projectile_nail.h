//====== Copyright � 1996-2005, Valve Corporation, All rights reserved. =======
//
// TF Nail Projectile
//
//=============================================================================
#ifndef TF_PROJECTILE_NAIL_H
#define TF_PROJECTILE_NAIL_H
#ifdef _WIN32
#pragma once
#endif

#include "cbase.h"
#include "tf_projectile_base.h"

#define NAIL_MODEL				"models/weapons/w_models/w_nail.mdl"
#define NAIL_DISPATCH_EFFECT	"ClientProjectile_Nail"
#define NAIL_GRAVITY	0.0f

#define DART_MODEL				"models/weapons/w_models/w_dart.mdl"
#define DART_DISPATCH_EFFECT	"ClientProjectile_Dart"
#define DART_GRAVITY			0.0f

#ifdef CLIENT_DLL
#define CTFProjectile_Nail C_TFProjectileNail 
#endif
//-----------------------------------------------------------------------------
// Purpose: Identical to a nail. five guys is awes ome
//-----------------------------------------------------------------------------
class CTFProjectile_Nail : public CTFBaseProjectile
{
	DECLARE_CLASS(CTFProjectile_Nail, CTFBaseProjectile);

public:

	CTFProjectile_Nail() {}
	~CTFProjectile_Nail() {}

	// Creation.
	static CTFProjectile_Nail* Create(const Vector& vecOrigin, const QAngle& vecAngles, CBaseEntity* pOwner = NULL, CBaseEntity* pScorer = NULL, bool bCritical = false);

	virtual const char* GetProjectileModelName(void) { return NAIL_MODEL; }
	virtual float GetGravity(void) { return NAIL_GRAVITY; }
	virtual const char* GetImpactEffect(void) { return "NailImpact"; }
	virtual bool IsNail() { return true; }

	static float	GetInitialVelocity(void) { return 1000.0; }

};

//-----------------------------------------------------------------------------
// Purpose: Identical to a dart. https://www.youtube.com/watch?v=8FWK4vcMgX4
//-----------------------------------------------------------------------------
#ifdef CLIENT_DLL
#define CTFProjectile_Dart C_TFProjectileDart
#endif

class CTFProjectile_Dart : public CTFBaseProjectile
{
	DECLARE_CLASS(CTFProjectile_Dart, CTFBaseProjectile);
	//DECLARE_NETWORKCLASS();

public:
	CTFProjectile_Dart() {}
	~CTFProjectile_Dart() 
	{
	#ifdef CLIENT_DLL
		ParticleProp()->StopEmission();
	#endif
	}
	// Creation.
#ifdef GAME_DLL
	static CTFProjectile_Dart* Create(const Vector& vecOrigin, const QAngle& vecAngles, CBaseEntity* pOwner = NULL, CBaseEntity* pScorer = NULL, bool bCritical = false);

	virtual const char* GetProjectileModelName(void) { return DART_MODEL; }
	virtual float	GetGravity(void) { return DART_GRAVITY; }
	static float	GetInitialVelocity( void ) { return 1000.0; }
	virtual void	ProjectileTouch( CBaseEntity* pOther );	
	virtual const char* GetImpactEffect(void) { return "DartImpact"; }
	virtual bool IsNail() { return true; }
#else
	virtual void CreateTrails( void );
#endif



};

#endif	//TF_PROJECTILE_NAIL_H