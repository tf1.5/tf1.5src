//====== Copyright � 1996-2005, Valve Corporation, All rights reserved. =======
//
// Purpose: 
//
//=============================================================================

#ifndef TF_WEAPON_BONESAW_H
#define TF_WEAPON_BONESAW_H
#ifdef _WIN32
#pragma once
#endif

#include "tf_weaponbase_melee.h"

#ifdef CLIENT_DLL
#define CTFBonesaw C_TFBonesaw
#define CTFSyringe C_TFSyringe
#endif

//=============================================================================
//
// Bonesaw class.
//
class CTFBonesaw : public CTFWeaponBaseMelee
{
public:

	DECLARE_CLASS( CTFBonesaw, CTFWeaponBaseMelee );
	DECLARE_NETWORKCLASS(); 
	DECLARE_PREDICTABLE();

	CTFBonesaw() {}
	virtual int			GetWeaponID( void ) const			{ return TF_WEAPON_BONESAW; }

private:

	CTFBonesaw( const CTFBonesaw & ) {}
};


class CTFSyringe : public CTFBonesaw
{
public:

	DECLARE_CLASS(CTFSyringe, CTFBonesaw);
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();

	CTFSyringe() {}
	virtual int			GetWeaponID(void) const { return TF_WEAPON_SYRINGE; }
#ifdef GAME_DLL
	virtual void		OnEntityHit( CBaseEntity* pOther );
#endif
private:

	CTFSyringe(const CTFSyringe&) {}
};

#endif // TF_WEAPON_BONESAW_H
