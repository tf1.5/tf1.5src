
//====== Copyright � 1996-2005, Valve Corporation, All rights reserved. =======
//
// TF Nail
//
//=============================================================================
#include "cbase.h"
#include "tf_projectile_nail.h"
#include "tf_weapon_tranq.h"

#ifdef GAME_DLL
	#include "tf_player.h"
#endif

#ifdef CLIENT_DLL
	#include "c_basetempentity.h"
	#include "c_te_legacytempents.h"
	#include "c_te_effect_dispatch.h"
	#include "input.h"
	#include "c_tf_player.h"
	#include "cliententitylist.h"
#endif



//=============================================================================
//
// TF Nail Projectile functions (Server specific).
//

LINK_ENTITY_TO_CLASS(tf_projectile_nail, CTFProjectile_Nail);
PRECACHE_REGISTER(tf_projectile_nail);

short g_sModelIndexNail;
void PrecacheNail(void* pUser)
{
	g_sModelIndexNail = modelinfo->GetModelIndex(NAIL_MODEL);
}

PRECACHE_REGISTER_FN(PrecacheNail);

//-----------------------------------------------------------------------------
// Purpose:
//-----------------------------------------------------------------------------
CTFProjectile_Nail* CTFProjectile_Nail::Create(const Vector& vecOrigin, const QAngle& vecAngles, CBaseEntity* pOwner, CBaseEntity* pScorer, bool bCritical)
{
	return static_cast<CTFProjectile_Nail*>(CTFBaseProjectile::Create("tf_projectile_nail", vecOrigin, vecAngles, pOwner, CTFProjectile_Nail::GetInitialVelocity(), g_sModelIndexNail, NAIL_DISPATCH_EFFECT, pScorer, bCritical));
}

#ifdef CLIENT_DLL

//-----------------------------------------------------------------------------
// Purpose: 
// Output : const char
//-----------------------------------------------------------------------------
const char* GetNailTrailParticleName(int iTeamNumber, bool bCritical)
{

	if (iTeamNumber == TF_TEAM_BLUE)
	{
		return (bCritical ? "nailtrails_medic_blue_crit" : "nailtrails_medic_blue");
	}
	else
	{
		return (bCritical ? "nailtrails_medic_red_crit" : "nailtrails_medic_red");
	}
}

//-----------------------------------------------------------------------------
// Purpose: 
//-----------------------------------------------------------------------------
void ClientsideProjectileNailCallback(const CEffectData& data)
{

	// Get the Nail and add it to the client entity list, so we can attach a particle system to it.
	C_TFPlayer* pPlayer = dynamic_cast<C_TFPlayer*>(ClientEntityList().GetBaseEntityFromHandle(data.m_hEntity));
	if (pPlayer)
	{

		C_LocalTempEntity* pNail = ClientsideProjectileCallback(data, NAIL_GRAVITY);
		if (pNail)
		{
			pNail->m_nSkin = (pPlayer->GetTeamNumber() == TF_TEAM_RED) ? 0 : 1;
			bool bCritical = ((data.m_nDamageType & DMG_CRITICAL) != 0);
			pNail->AddParticleEffect(GetNailTrailParticleName(pPlayer->GetTeamNumber(), bCritical));
			pNail->AddEffects(EF_NOSHADOW);
			pNail->flags |= FTENT_USEFASTCOLLISIONS;
		}
	}
}

DECLARE_CLIENT_EFFECT(NAIL_DISPATCH_EFFECT, ClientsideProjectileNailCallback);
#endif


//=============================================================================
//
// TF Dart Projectile functions (Server specific).
//

LINK_ENTITY_TO_CLASS(tf_projectile_dart, CTFProjectile_Dart);
PRECACHE_REGISTER(tf_projectile_dart);

short g_sModelIndexDart;

void PrecacheDart(void* pUser)
{
	g_sModelIndexDart = modelinfo->GetModelIndex(DART_MODEL);
}

PRECACHE_REGISTER_FN( PrecacheDart )
#ifdef GAME_DLL
CTFProjectile_Dart* CTFProjectile_Dart::Create( const Vector& vecOrigin, const QAngle& vecAngles, CBaseEntity* pOwner /*= NULL*/, CBaseEntity* pScorer /*= NULL*/, bool bCritical /*= false */ )
{
	return static_cast<CTFProjectile_Dart*>(CTFBaseProjectile::Create( "tf_projectile_dart", vecOrigin, vecAngles, pOwner, CTFProjectile_Nail::GetInitialVelocity(), g_sModelIndexDart, DART_DISPATCH_EFFECT, pScorer, bCritical ) );
}

void CTFProjectile_Dart::ProjectileTouch(CBaseEntity* pOther)
{
	CTFPlayer* pPlayer = ToTFPlayer(pOther);
	if (pPlayer && pPlayer->GetTeamNumber() != GetOwnerEntity()->GetTeamNumber() && !pPlayer->m_Shared.InCond(TF_COND_INVULNERABLE))
	{
		//if they're already tranquilized add 2 seconds onto the current duration
		if (pPlayer->m_Shared.InCond(TF_COND_TRANQUILIZED))
			pPlayer->m_Shared.AddCond(
				TF_COND_TRANQUILIZED,
				Clamp<float>(
					pPlayer->m_Shared.GetConditionDuration(TF_COND_TRANQUILIZED) + 2.0f, 0.0f, TRANQ_TIME
					));
		else
			pPlayer->m_Shared.AddCond(TF_COND_TRANQUILIZED, TRANQ_TIME);
	}
	BaseClass::ProjectileTouch(pOther);
}
#endif // GAME_DLL


#ifdef CLIENT_DLL
void CTFProjectile_Dart::CreateTrails( void )
{
	if ( IsDormant() )
		return;

	if (GetTeamNumber() == TF_TEAM_BLUE)
	{
		ParticleProp()->Create("tranq_tracer_teamcolor_blue", PATTACH_ABSORIGIN_FOLLOW);
	}
	else
	{
		ParticleProp()->Create("tranq_tracer_teamcolor_red", PATTACH_ABSORIGIN_FOLLOW);
	}

}
#endif
